package com.fastgen.core.base;

/**
 * 常量
 *
 * @author: zet
 * @date:2019/9/12
 */
public class Contants {
    public static final String USER_CFG = "user-cfg.properties";//生成配置的配置文件
    public static final String TEMPLATES_PATH_NAME = "templates";//模板配置文件
    public static final String TAG_START_CONFIG = "<#--start_config-->";//起始标记
    public static final String TAG_END_CONFIG = "<#--end_config-->";//结束标记

    public static final String FTL_CONFIG_FILE_PATH = "filePath";
    public static final String FTL_CONFIG_ENABLE = "enable";

    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String ENV_PROD = "prod";//生成环境
    public static final String CHARSET_CODE = "utf-8";//默认编码
    public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";//临时目录
    public static final String PREFIX_SRC_MAIN_JAVA = "\\\\src\\\\main\\\\java";
    public static final String PREFIX_SRC_MAIN_RESOURCES = "\\\\src\\\\main\\\\resources";
}
